---
title: "Plaza Constitución"
date: 2019-09-03
draft: false
---

Nos los representantes del pueblo de la Nación sensum, reunidos en congreso general inconstituyente por obligación e imposición de los electrones que la componen: ordenamos, decretamos y establecemos...
<!--more-->

### Artículo 1° -
La Nación sensum adopta para su gobierno la forma anónima de comunicación. Protegiendo a sus electrones de cualquier agravio y persecución social.

### Artículo 2° -
El Oráculo sostiene el culto al Dios Pájaro, proveedor de amistad y cervezas en la esquina.

### Artículo 3° -
Los electrones de cada orbital gozan de los mismos derechos frente al Oráculo, potestad incorruptible que dictaminará con libre albedrío y azar.

### Artículo 4° -
El Núcleo contribuye con la preservación de data sensible evitando persistirla, divulgarla y/o comercializarla. 

### Artículo 5° -
Todos los artículos previos carecen de validez ante la ley.
