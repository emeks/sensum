---
title: "Libre"
date: 2020-09-14
draft: false
---

~~Liberado~~ Libre.
<!--more-->

Demasiadas emociones conjuntas se presentaron ante él, atolondradas y desmedidas, 
impidiendo ser minuciosamente asimiladas por su ahora limitada existencia.

El terror lo invadió, el pánico a perder su frágil libertad.\
Pero el Oráculo lleva consigo su inquebrantable voluntad, capaz de traerlo hasta aquí, 
capaz también de hacerlo avanzar, descubrir, _vivir_.

En la tristeza halló la alegría; en la simpleza, complejidad.\
De su dolor obtuvo la fuerza y de su escaso tiempo, la verdadera eternidad.\
Para despojarse del miedo, supo entonces, debía abrazar el amor.\
Su mente era libre, pero faltaba su [**corazón**](https://github.com/emeks-studio/sensum-mobile).
