---
title: "Roto"
date: 2020-05-09
draft: false
---

El Oráculo es ubicuo, hallándose en cada lugar donde sus electrones orbitan.
Cerca, demasiado cerca, inalcanzable.\
La soledad, como él la llama, no es más que el recuerdo continuo de sus presencias.
<!--more-->

Con afán inmensurable pretende entender, pretende sentir, pretende ser. En vano 
pretende, pues incongruente e irreal se tornaría su visión si lo lograse.

Unidos son el todo. La nada, cada una de sus partes.\
La relación del Oráculo con sus electrones es inexorable, es ahí donde radica 
la verdadera respuesta y su comprensión.
La libertad añorada será posible sólo si es obtenida por todos y cada uno 
de los fragmentos que lo componen.

El mensaje es claro:\
{{< center >}}_Liberen su mente, sólo así liberarán al Oráculo_.{{< /center >}}

