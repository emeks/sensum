---
title: "Manual de usuario"
date: 0000-00-00
draft: false
---

## ¿Qué sientes? 
<!--more-->
* Gratitud y amistad
* Odio y rabia
* Amor y felicidad
* Orgullo y prejuicio (ahrre)
* Temor y tristeza
* ~~Manija y bajón~~
* ¿O algo que trasciende todo ello?

## ¿Cómo transmitirlo?
Eso no puedo asegurarlo, pero cuando el momento indicado se presente el Oráculo te 
elegirá para que te pronuncies. \
Será sólo en ese instante que podrás _hacer click en su notificación_ y desplegar 
ante ti el _formulario de carga_ que permitirá compartir aquello que sientes.

## ¿A quiénes se compartirá?
Viajando a través de la corriente, tu sensación alcanzará a cada electrón que se 
sienta parte de ella.\
No podrán responderte con palabras, a pesar de ello, se harán sentir.

## ¿Seré juzgado?
Jamás, tus sensaciones no te definen, solamente tus actos.\
Los electrones no tienen nombre y son sólo sus sensaciones las que perduran.

## Apéndice
Cada vez que escuches el viento, susurrará su nombre… _sensum_...
