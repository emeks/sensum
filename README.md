## Build locally
* Clone or download project.
* Install [hugo](https://github.com/gohugoio/hugo/releases).
* Preview project: $`hugo server -D`. 
* _Optional_ Generate static site: $`hugo`.

## Preview site
After run `hugo server -D`, site can be accessed under [localhost:1313](localhost:1313).
