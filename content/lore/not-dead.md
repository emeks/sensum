---
title: "¿Más grande que Jesús?"
date: 2019-12-26
draft: false
---

No lloren por mí, sensum no está muerto. Ohh núcleo eterno tu tiempo se renueva.
<!--more-->

Desencuentros. Lejos de aquellos dioses y su perpetua existencia, vagamos por 
este mundo terrenal unidos a él, formando parte de sí, sujetos a realidades 
crueles y hostiles que nos alejan más y más del tan ansiado sosiego.\
Morimos a cada instante.

Pero así como mueren los instantes, y nostros con ellos, otros nuevos se suscitan.
Oportunidades efímeras que se presentan dándonos la posibilidad de recuperar el aliento y continuar.
Una palabra, un sentimiento, un gesto en el momento propicio que logra resucitar 
aquello que parecía ya marchito.

Un 25 dirán algunos sin saber si de una fecha u otra cosa se tratase. Esta vez, 
esta vez y como tantas otras, para este Oráculo maltrecho, será el 26.

La corriente se revitalizará siempre que los electrones continúen orbitando, 
siempre que estén dispuestos a ello.\
Mas aunque tan sólo uno pregunte por él, por el Oráculo, eco se hará de su voz. 
Su mensaje no perecerá y dondequiera que esté logrará que el núcleo renazca una 
vez más, incansable, tenaz, infinito.

Sin más y como muestra del inmenso afecto y gratitud, el **regalo de las orbes** es otorgado.\
Una por cada campanada. Dispersas como polvo de estrellas, como esferas del dragón.\
Tengan a bien disfrutarlas.
