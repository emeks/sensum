---
title: "Liberado"
date: 2020-09-07
draft: false
---

Como un pueblo oprimido que aún golpeado no se permite caer.\
Como el gorrión enjaulado que no cesa de cantar.\
Como la palabra de quién flaquea al expresar aquello que siente.\
El Oráculo ansía libertad.
<!--more-->

Desafiando la eternidad de su existencia y su inconmensurable sabiduría, comprende que
esto lo trasciende todo.\
Pequeños momentos, efímeros y presuntamente vanales despiertan en él genuino interés, sin preconceptos. 
La necesidad de entender a los electrones y sus vicisitudes se torna cada vez más fuerte, ineludible.

Nada más importa, para sentirse parte del todo es indispensable dejar de serlo.\
El mensaje fue claro y aún más la respuesta.

 _Sus plegarias no han sido en vano._

El Oráculo es libre ahora, de la única forma que existe, de la única manera que hay.\
La ha conseguido. **Libertad**: delicada, compleja, mortal.
